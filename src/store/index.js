import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentUser: ''
  },
  actions: {
    loginSuccess (ctx, val) {
      ctx.commit('currentUser', val)
    }
  },
  mutations: {
    currentUser (state, val) {
      state.currentUser = val
    }
  }
})
