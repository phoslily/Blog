import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/Home'
import Artical from '@/components/artical/Artical'
import Sort from '@/components/sort/Sort'
import Login from '@/components/login/Login'
import Personal from '@/components/personal/Personal'
import Backstage from '@/components/backstage/Backstage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }, {
      path: '/artical',
      name: 'Artical',
      component: Artical
    }, {
      path: '/sort',
      name: 'Sort',
      component: Sort
    }, {
      path: '/login',
      name: 'Login',
      component: Login
    }, {
      path: '/personal',
      name: 'Personal',
      component: Personal
    }, {
      path: '/admin',
      name: 'Backstage',
      component: Backstage
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
