// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueAwesineSwiper from 'vue-awesome-swiper'
import {Form, FormItem, Input, Button, Checkbox, Tabs, TabPane, RadioGroup, RadioButton, Upload, Pagination, CheckboxGroup, Table, TableColumn} from 'element-ui'
import axios from 'axios'
import 'styles/normalize.css'
import 'swiper/dist/css/swiper.css'
import 'styles/iconfont.css'
import 'element-ui/lib/theme-chalk/index.css'

Vue.config.productionTip = false
Vue.prototype.axios = axios
Vue.use(VueAwesineSwiper)
Vue.prototype.$ELEMENT = { size: 'small', zIndex: 10 }
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Button)
Vue.use(Checkbox)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(RadioGroup)
Vue.use(RadioButton)
Vue.use(Upload)
Vue.use(Pagination)
Vue.use(CheckboxGroup)
Vue.use(Table)
Vue.use(TableColumn)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  mounted () {
    var docEl = document.documentElement
    var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
    var recalc = function () {
      docEl.style.fontSize = 20 * (docEl.clientWidth / 320) + 'px'
    }
    window.addEventListener(resizeEvt, recalc, false)
    document.addEventListener('DOMContentLoaded', recalc, false)
  }
})
